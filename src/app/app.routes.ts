
import { Routes, RouterModule, CanActivate } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { AppMainComponent } from './layout/app.main.component';
import { AppNotfoundComponent } from './pages/app.notfound.component';
import { AppErrorComponent } from './pages/app.error.component';
import { AppAccessdeniedComponent } from './pages/app.accessdenied.component';
import { AppLoginComponent } from './pages/app.login.component';
import { AppInvoiceComponent } from './pages/app.invoice.component';
import { AppHelpComponent } from './pages/app.help.component';
import { AppWizardComponent } from './pages/app.wizard.component';
import { AuthenticationGuard } from './core/guards/authentication.guard';
import { LayoutDashboardComponent } from './layout-dashboard/dashboard.layout';

export const routes: Routes = [
    { path: '', component: AppMainComponent, canActivate: [AuthenticationGuard] ,

        children: [

            { path: '', component: AppInvoiceComponent },
            { path: 'help', component: AppHelpComponent },
            // { path: 'examplewithAuthentication', component: EventoListComponent,canActivate: [AuthenticationGuard]},
        ],

    },

    {
        path: 'login',
        loadChildren: './core/login/login.module#LoginModule'

    },
    {path: 'error', component: AppErrorComponent},
    {path: 'accessdenied', component: AppAccessdeniedComponent},
    {path: 'notfound', component: AppNotfoundComponent},
    {path: 'loginpage', component: AppLoginComponent},
    {path: 'wizard', component: AppWizardComponent},
    {path: '**', redirectTo: '/notfound'},
    

];

export const AppRoutes: ModuleWithProviders = RouterModule.forRoot(routes, {scrollPositionRestoration: 'enabled'});
