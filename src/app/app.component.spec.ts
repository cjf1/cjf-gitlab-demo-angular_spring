/* tslint:disable:no-unused-variable */

import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { AppTopBarComponent } from './layout/app.topbar.component';
import { ProfileComponent } from './layout/profile/profile.component';
import { FooterComponent } from './core/footer/footer.component';
import { AppMenuComponent, AppSubMenuComponent } from './layout/app.menu.component';
import { ScrollPanelModule } from 'primeng/scrollpanel';

describe('AppComponent', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
        imports: [ RouterTestingModule, ScrollPanelModule ],
        declarations: [ AppComponent,
                AppTopBarComponent,
                AppMenuComponent,
                AppSubMenuComponent,
                FooterComponent,
                ProfileComponent
            ],
    });
    TestBed.compileComponents();
  });

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
});
