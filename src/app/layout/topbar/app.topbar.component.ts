import {Component, Inject, HostListener} from '@angular/core';
import {AppMainComponent} from '../app.main.component';
import { Observable } from 'rxjs';
import { User } from '../../core/models/user';
import { UserService } from '../../core/services/user.service';
import { Router } from '@angular/router';
import { trigger, state, transition, style, animate } from '@angular/animations';  
import { DOCUMENT } from '@angular/common';

@Component({
    selector: 'app-topbar',
    templateUrl: './app.topbar.component.html',
    styleUrls: ['./app.topbar.component.css'],
    animations:[ 
      trigger('fade',
      [ 
        state('void', style({ opacity : 0})),
        transition(':enter',[ animate(300)]),
        transition(':leave',[ animate(500)]),
      ]
  )]
})
export class AppTopBarComponent {
    user$: Observable<User>;
    
    constructor(public app: AppMainComponent,private userService: UserService, private router:Router, @Inject(DOCUMENT) document) {}

    


    ngOnInit(): void {
     this.user$ = this.userService.getUser();

    }

    @HostListener('window:scroll', ['$event'])
    onWindowScroll(e) {
       if (window.pageYOffset > 114) {
        this.app.setSticky(true);
       } else {
        this.app.setSticky(false);
       }
    }

    logout(){
        this.userService.logout()
        this.redirectToLogin();
        setTimeout(() => {
            this.redirectToLogin();
        }, 300 );

    }

    redirectToLogin(){
        this.router.navigate(['/login']);
            
    }

}
