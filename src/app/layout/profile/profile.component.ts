import {Component, OnInit} from '@angular/core';
import {trigger, state, transition, style, animate} from '@angular/animations';
import { UserService } from '../../core/services/user.service';
import { Router } from '@angular/router';
import { User } from 'src/app/core/models/user';
import { Observable } from 'rxjs';

@Component({
    selector: 'cjf-inline-profile',
    templateUrl:'./profile.component.html' ,
    animations: [
        trigger('menu', [
            state('hidden', style({
                height: '0px'
            })),
            state('visible', style({
                height: '*'
            })),
            transition('visible => hidden', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)')),
            transition('hidden => visible', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)'))
        ])
    ]
})
export class ProfileComponent implements OnInit {


    user$: Observable<User>;


    ngOnInit(): void {
     this.user$ = this.userService.getUser();

    }

    
    constructor(private userService: UserService, private router:Router){}

    active: boolean;

    onClick(event) {
        this.active = !this.active;
        event.preventDefault();
    }

    logout(){
        this.userService.logout()
        this.redirectToLogin();
        setTimeout(() => {
            this.redirectToLogin();
        }, 1000 );

    }

    redirectToLogin(){
        this.router.navigate(['/login']);
            
    }
}
