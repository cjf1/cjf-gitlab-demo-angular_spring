import { NgModule } from '@angular/core';
import { SignInComponent } from './signin/signin.component';
import { ReactiveFormsModule, FormsModule} from '@angular/forms';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SignUpComponent } from './signup/signup.component';
import { LoginComponent } from './login.component';
import { LoginRoutingModule } from './login.routing.module';
import { SignUpService } from './signup/signup.service';
import { CjfSharedModule } from 'cjf-shared';

@NgModule({
    declarations: [ SignInComponent, SignUpComponent, LoginComponent ],
    exports:[SignInComponent,SignUpComponent],
    imports:[FormsModule,ReactiveFormsModule, CommonModule, RouterModule,LoginRoutingModule, CjfSharedModule],
    providers:[SignUpService]
})
export class LoginModule { }