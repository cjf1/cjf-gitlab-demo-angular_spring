import { Component, OnInit, ElementRef, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/core/auth/auth.service';
import { Router } from '@angular/router';
import { PlatformDetectorService } from 'src/app/core/services/platform-detector.service';
//import $ from 'jquery';
import config from '../../../../assets/config/config.json'





@Component({
    templateUrl:'./signin.component.html',
    styleUrls:  ['./signin.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class SignInComponent implements OnInit{

    loginForm: FormGroup;
    registerForm: FormGroup;

    infoBuild: any = config;

    text: string = "aa"

  public value: string[];


    @ViewChild('userNameInput', {static: false}) userNameInput: ElementRef<HTMLInputElement>;

    constructor(
        private formBuilder: FormBuilder, 
        private authService: AuthService,
        private router: Router,
        private platformDetectorService: PlatformDetectorService
    ){}


    ngOnInit(): void{
        this.loginForm = this.formBuilder.group({
            userName:['', Validators.required],
            password:['', Validators.required]
         } );

         this.registerForm = this.formBuilder.group({
            userName:['', Validators.required],
            password:['', Validators.required],
            email:['', Validators.required]
         } );
      
          this.value = ['multiple2', 'multiple4'];
    
  
    }



    eventoClick(evento){
      //  $('form').animate({height: "toggle", opacity: "toggle"}, "slow");
    }

    login(){

        const userName = this.loginForm.get('userName').value;
        const password = this.loginForm.get('password').value;

        console.log("vai autenticar.");

        this.authService
            .authenticate(userName,password)
            .subscribe(
                ()=> this.router.navigate(['/']), 
                err=>{
                    console.log(err);
                    this.loginForm.reset();
                    alert("Usuário ou senha incorretos.")
                    this.platformDetectorService.isPlatformBrowser() &&
                        this.userNameInput.nativeElement.focus();
                    
                }
            );
    }

    signup(){
        this.router.navigate(['/signup']);
    }

}