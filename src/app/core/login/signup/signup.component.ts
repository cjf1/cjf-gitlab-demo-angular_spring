import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { lowerCaseValidator } from 'src/app/shared/validators/lower-case.validator';
import { UserNotTakenValidadorService } from './user-not-taken.validator.service';
import { NewUser } from './new-user';
import { SignUpService } from './signup.service';
import { Router } from '@angular/router';
//import $ from 'jquery';
import { UserService } from 'src/app/core/services/user.service';

@Component({
    templateUrl:'./signup.component.html',
    selector:'ap-signup',
    styleUrls:['./signup.component.css'],
    providers:[UserNotTakenValidadorService]
    
})
export class SignUpComponent implements OnInit{

    signupForm: FormGroup;
    submitted = false;
    constructor(private formBuilder: FormBuilder, private userNotTakenValidatorService: UserNotTakenValidadorService, private signUpService: SignUpService,
        private router: Router){}

    ngOnInit():void {
        this.signupForm = this.formBuilder.group({
           // userName:   ['', [Validators.required,Validators.minLength(2), Validators.maxLength(30), Validators.pattern(/^[a-z0-9_\-]+$/)]],
           userName:   ['', [Validators.required,Validators.minLength(2), Validators.maxLength(30), lowerCaseValidator], this.userNotTakenValidatorService.checkUserNameTaken()],
            fullName:   ['', [Validators.required,Validators.minLength(2), Validators.maxLength(40)]],
            email:      ['', [Validators.required,Validators.email]],
            password:   ['', [Validators.required,Validators.minLength(8), Validators.maxLength(14)]]
        })
    }

    signUp(){
        const newUser = this.signupForm.getRawValue() as NewUser;
        this.signUpService
            .signup(newUser)
            .subscribe(
                ()=>{
                    this.router.navigate(['']);
                    this.eventoClick(null);
                },
                err => console.log(err)
            )
    }

    eventoClick(evento){
       // $('form').animate({height: "toggle", opacity: "toggle"}, "slow");
    }

    

}