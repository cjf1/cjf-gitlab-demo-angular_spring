import { Component } from "@angular/core";
import { AppMainComponent } from 'src/app/layout/app.main.component';

@Component({
    selector: 'cjf-header',
    templateUrl:'./header.component.html',
    styleUrls:['./header.component.css']
})
export class HeaderComponent {
    sticky: boolean = false;

    constructor(){}
}