import { NgModule, ErrorHandler } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { RequestInterceptor } from './auth/request.interceptor';
import { ProfileComponent } from '../layout/profile/profile.component';
import { AuthenticationGuard } from './guards/authentication.guard';
import { HeaderComponent } from './header/header.component';


@NgModule({
    declarations: [ ProfileComponent, HeaderComponent],
    exports:[ ProfileComponent, HeaderComponent],
    imports: [CommonModule, RouterModule],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: RequestInterceptor,
            multi: true
        },AuthenticationGuard,
        AuthenticationGuard.guards
    ]
})
export class CoreModule{

}