import { Injectable } from '@angular/core';
import { TokenService } from './token.service';
import { BehaviorSubject } from 'rxjs';
import { User } from '../models/user';
import * as jwt_decode from 'jwt-decode';

@Injectable({providedIn:'root'})
export class UserService{
    private userName = '';
    private id=0;
    private userSubject = new BehaviorSubject<User>(null);
    constructor(private tokenService: TokenService){
       this.tokenService.hasToken() && this.decodeAndNotify();
    }

    setToken(token: string ){
        this.tokenService.setToken(token);
        this.decodeAndNotify();

    }

    getUser(){
        return this.userSubject.asObservable();
    }
    getUserId(){
        return this.id;
    }

    private decodeAndNotify(){
        const token = this.tokenService.getToken();
        const user = jwt_decode(token) as User;
        this.userName = user.nome;
        this.id = user.sub;
        this.userSubject.next(user);

    }

    logout(){
        this.tokenService.removeToken();
        this.userSubject.next(null);
    }

    isLogged(){
        return this.tokenService.hasToken();
    }

    getUserName(){
        return this.userName;
    }
}