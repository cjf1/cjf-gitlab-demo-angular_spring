import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { UserService } from '../services/user.service';
import { Token } from '../models/token';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  enviromentUrl = '';

  constructor(
    private http: HttpClient,
    private userService: UserService
    
    ) { 

      this.enviromentUrl = environment.apiUrl;
    }


    authenticate(userName: string, password: string ){
      return this.http
        .post(
          this.enviromentUrl+'/auth',
          {login:userName, senha:password},
          {observe: 'response'}
        )
        .pipe(tap(res=>{
          console.log(res.body);
          const authToken = (res.body as Token ).token;
          this.userService.setToken(authToken);
          
          console.log(`User ${userName} authenticated with token ${authToken}`)
        }))
        ;

    }
  
}
