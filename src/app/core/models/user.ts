export interface User{
    id: number;
    nome: string;
    email: string;
    sub: number;
    idParticipante: number;
}