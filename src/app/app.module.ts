import {NgModule, APP_INITIALIZER} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HashLocationStrategy, LocationStrategy} from '@angular/common';
import {AppRoutes} from './app.routes';


import {AppComponent} from './app.component';
import {AppMainComponent} from './layout/app.main.component';
import { AppConfigComponent } from './layout/app.config.component';
import {AppNotfoundComponent} from './pages/app.notfound.component';
import {AppErrorComponent} from './pages/app.error.component';
import {AppAccessdeniedComponent} from './pages/app.accessdenied.component';
import {AppLoginComponent} from './pages/app.login.component';
import {AppMenuComponent, AppSubMenuComponent} from './layout/app.menu.component';
import {AppTopBarComponent} from './layout/topbar/app.topbar.component';
import {AppInvoiceComponent} from './pages/app.invoice.component';
import {AppHelpComponent} from './pages/app.help.component';
import {AppWizardComponent} from './pages/app.wizard.component';
import { CoreModule } from './core/core.module';

import { NotificationsService } from './shared/notifications/notifications.service';
import { SharedModule } from './shared/shared.module';


import { ScrollPanelModule} from 'primeng/scrollpanel';
import {  TabViewModule} from 'primeng/tabview';
import { AccordionModule} from 'primeng/accordion';
import { DropdownModule} from 'primeng/dropdown';
import {  CalendarModule } from 'primeng/calendar';
import {  ButtonModule } from 'primeng/button';
import {CheckboxModule } from 'primeng/checkbox';
import { AppConfig } from './app.config';
import { LayoutDashboardComponent } from './layout-dashboard/dashboard.layout';
import { CjfSharedModule } from 'cjf-shared'
import { LoaderService } from 'cjf-shared';

export function initConfig(config: AppConfig){
    return () => config.loadConfigs()
  }


@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        AppRoutes,
        HttpClientModule,
        BrowserAnimationsModule,
        ScrollPanelModule,
        TabViewModule ,
        AccordionModule,
        CheckboxModule,
        DropdownModule,
        CalendarModule,
        ButtonModule,
        CoreModule,
        SharedModule,
        CjfSharedModule,
    ],
    declarations: [
        AppComponent,
        AppMainComponent,
        AppMenuComponent,
        AppSubMenuComponent,
        AppTopBarComponent,
        AppConfigComponent,
        AppNotfoundComponent,
        AppErrorComponent,
        AppAccessdeniedComponent,
        AppLoginComponent,
        AppInvoiceComponent,
        AppHelpComponent,
        AppWizardComponent,
        LayoutDashboardComponent
    ],
    providers: [
        AppConfig,
        { provide: APP_INITIALIZER, useFactory: initConfig, deps: [AppConfig], multi: true },
        {provide: LocationStrategy, useClass: HashLocationStrategy},
        LoaderService,
        NotificationsService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
