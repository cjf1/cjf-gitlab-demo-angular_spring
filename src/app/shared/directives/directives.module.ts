import { NgModule } from '@angular/core';
import { BackButtonDirective } from './back-button.directive';
import { AutofocusDirective } from './autofocus.directive';

@NgModule({
    declarations: [ BackButtonDirective, AutofocusDirective ],
    exports: [ BackButtonDirective, AutofocusDirective ]
})
export class DirectivesModule { }
