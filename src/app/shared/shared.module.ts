import {  MessagesModule  } from 'primeng/messages';
import {  MessageService } from 'primeng/api';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { NotificationsComponent } from './notifications/notifications.component';
import { NotificationsService } from './notifications/notifications.service';
import { ToastModule } from 'primeng/toast';
import { CommonModule } from '@angular/common';


@NgModule({
  imports: [ToastModule, MessagesModule,CommonModule],
  declarations: [
     NotificationsComponent,
     ],
  providers: [
    MessageService 
  ],
  exports: [
    NotificationsComponent,
  ]

})



export class SharedModule {
  
}
