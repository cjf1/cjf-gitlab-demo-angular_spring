# Configuração do projeto

1) Faça uma cópia do conteúdo deste repositório para servir de projeto base na construção de sua aplicação frontend em angular.

2) Abra o arquivo Angular.json, localizado na raiz do projeto, e execute um ReplaceAll alterando a o termo "nome_projeto" para o nome de sua aplicação.

3) Repita o passo 2 no arquivo Package.json, localizado na raiz do projeto.

4) Para modificar o título das páginas htmls criadas, altere no arquivo src/index.html o conteúdo "Título Projeto" pelo título desejado.

5) Altere o apontamento do endpoind da sua api no arquivo src/environments/environment.ts

6) Execute o comando: `npm install`

7) Teste a aplicação com o comando: `ng serve`

# PrimeCli

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.3.0.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
